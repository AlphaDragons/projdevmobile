# Simon Says Stuff
Projet DevMobile réalisé par DI LORETO Romain & FLORENT Victor

# How to
## Formulaire d'inscription et connexion classique

Nota Bene : POUR LA DATE DE NAISSANCE, VEUILLEZ DOUBLE TAPER LE CHAMP afin d'afficher le calendrier

## Menu

Affichage d'un leaderboard du top 10 des utilisateurs avec les meilleurs scores (Vous-même pouvant y être compris)
En cas d'égalité, chaque personne aura le même rang, puis le comptage remprend son cours. (Par exemple en cas d'égalité pour la première place, le classement sera 1er, 1er puis 3ème etc...) 

## Section jeu

Trois modes : Normal, Difficile et Expert avec un nombre de flash min/max différents.
Plus difficile = Plus de points, évidemment.

Jeu de Simon classique : mémorisez une séquence de flashs et retranscrivez-la. Rien de plus simple!

Nota Bene : Cliquer sur un bouton ne change pas son apparance malgré l'utilisation de fichiers xml permettant cela.
