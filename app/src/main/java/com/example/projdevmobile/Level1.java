package com.example.projdevmobile;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

@RequiresApi(api = Build.VERSION_CODES.N)
public class Level1 extends GameMechanics {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);
        level=1;
        initGame();
    }

    @Override
    protected Intent NewLevel(String whatNext){
        try {
            Intent intent;
            switch (whatNext) {
                case "next":
                    intent = new Intent(Level1.this, Level2.class);
                    break;
                case "restart":
                    intent = new Intent(Level1.this, Level1.class);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            Bundle bundle = new Bundle();
            bundle.putIntArray("Blinking_Times", new int[]{blinkMin, blinkMax});
            bundle.putInt("Lives", livesMax);
            bundle.putFloat("Weight", coeff);
            bundle.putSerializable("User",user);
            intent.putExtras(bundle);
            return intent;
        }
        catch(IllegalArgumentException exception){
            return null;
        }
    }
}