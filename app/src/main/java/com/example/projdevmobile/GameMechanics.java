package com.example.projdevmobile;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@RequiresApi(api = Build.VERSION_CODES.N)
public abstract class GameMechanics extends AppCompatActivity {

    protected int blinkMin;
    protected int blinkMax;
    protected int livesMax;
    protected float coeff;
    protected int level;
    private int cpt;
    protected User user;

    private ArrayList<Integer> buttonList;

    private ArrayList<Integer> flashList;
    private int advance;
    private int lives;

    private Intent intent;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();
        bundle = intent.getExtras();
        blinkMin=bundle.getIntArray("Blinking_Times")[0];
        blinkMax=bundle.getIntArray("Blinking_Times")[1];
        livesMax=bundle.getInt("Lives");
        coeff=bundle.getFloat("Weight");
        user=(User)bundle.getSerializable("User");
        advance=0;
        lives=livesMax;
    }

    protected void initGame(){
        if(livesMax!=3){
            findViewById(R.id.life3).setVisibility(View.GONE);
        }

        flashList=new ArrayList<Integer>();

        buttonList = new ArrayList<Integer>();
        findViewById(R.id.buttonGroup).getTouchables().forEach(butt->{
            buttonList.add(butt.getId());
            butt.setOnTouchListener((v, event) -> {
                if(event.getAction()==MotionEvent.ACTION_DOWN)
                        onPress(butt);
                return false;
            });
        });

        StartGame();
    }

    private void onPress(View button){
        //button.setBackground(getDrawable(getResources().getIdentifier(button.getTag().toString()+"press","drawable", getApplicationContext().getPackageName())));
        if(button.getId()== flashList.get(advance)){
            if(++advance==flashList.size()) { //If all flashes done
                if(flashList.size()==blinkMax){ //If last stage
                    user.setScore(coeff*level);
                    Intent nextLevel = NewLevel("next");
                    startActivity(nextLevel);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Good!",Toast.LENGTH_SHORT).show();
                    advance=0;
                    newFlash();
                }
            }
        }
        else{
            if(--lives==0){
                Intent restartLevel = NewLevel("restart");
                startActivity(restartLevel);
            }
            else{
                Toast.makeText(getApplicationContext(),"Ho no!",Toast.LENGTH_SHORT).show();
                ((ImageView)findViewById(getResources().getIdentifier("life"+(lives+1),"id", getApplicationContext().getPackageName()))).setImageResource(R.drawable.lifeoff);
                advance=0;
                StartSequence();
            }

        }
    }

    private void StartSequence(){
        ((TextView)findViewById(R.id.scoreDisplay)).setText(R.string.ai_turn);
        Toast.makeText(getApplicationContext(),"Look!",Toast.LENGTH_SHORT).show();
        buttonList.forEach(b->findViewById(b).setEnabled(false)); //Deactivate all buttons
        cpt=0;
        flashList.forEach(fl->{

            cpt++;

            new Handler().postDelayed(new Runnable() {
                boolean forListEnd=cpt==flashList.size();
                @Override
                public void run() {
                    new CountDownTimer(1000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            findViewById(fl).setBackground(getDrawable(getResources().getIdentifier(findViewById(fl).getTag().toString()+"press","drawable", getApplicationContext().getPackageName())));
                        }

                        public void onFinish() {
                            findViewById(fl).setBackground(getDrawable(getResources().getIdentifier(findViewById(fl).getTag().toString(),"drawable", getApplicationContext().getPackageName())));
                            if(forListEnd){
                                buttonList.forEach(b->findViewById(b).setEnabled(true)); //Reactivate all buttons
                                Toast.makeText(getApplicationContext(),"Your turn!",Toast.LENGTH_SHORT).show();
                                ((TextView)findViewById(R.id.scoreDisplay)).setText(R.string.player_turn);
                            }
                        }
                    }.start();
                }
            }, 1100*cpt);
        });

    }

    private void StartGame(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new CountDownTimer(1000, 1) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        InitiateSequence();
                    }
                }.start();
            }
        }, 3000);
    }

    private void newFlash(){
        Random rng = new Random();
        flashList.add( buttonList.get( Math.abs(rng.nextInt() % buttonList.size()) ) );
        StartSequence();
    }

    private void InitiateSequence(){
        Random rng = new Random();
        for(int i=1;i<=blinkMin;i++){
            flashList.add( buttonList.get( Math.abs(rng.nextInt() % buttonList.size()) ) );
        }
        StartSequence();
    }

    protected abstract Intent NewLevel(String whatNext);
}