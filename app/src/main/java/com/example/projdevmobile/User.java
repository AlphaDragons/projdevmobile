package com.example.projdevmobile;


import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;

public class User implements Serializable, Comparable<User> {
    private String lastName, firstName, birthdate, genre, id;
    private final String TAG = "BDDInfo";
    private float Score;


    public User() {}

    public User(String id, String lastname, String firstname, String birthdate, String genre, float score) {
        this.id = id;
        this.lastName = lastname;
        this.firstName = firstname;
        this.birthdate = birthdate;
        this.genre = genre;
        this.Score = score;
    }

    public User(String lastname, String firstname, String birthdate, String genre, float score)
    {this(null, lastname, firstname, birthdate, genre, score);}

    public String getLastName()
    {
        return lastName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getBirthdate()
    {
        return birthdate;
    }

    public String getGenre()
    {
        return genre;
    }

    public float getScore()
    {
        return Score;
    }

    public void setScore(float newScore)
    {
        if(id.isEmpty()) return;
        Score += newScore;
        this.update();
    }

    public void update()
    {
        if(id.isEmpty()) return;
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("users").document(id)
                .set(this)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }


    @Override
    public int compareTo(User o) {
        float score1 = this.getScore();
        float score2 = o.getScore();
        return Float.compare(score1,score2);
    }
}
