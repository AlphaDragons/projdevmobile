package com.example.projdevmobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class GameMenu extends AppCompatActivity {

    public User user;

    private ArrayList<User> leaderboard;
    private RecyclerView recyclerView;
    private RankAdapter rankAdapter;
    private Intent intent;
    private Bundle bundle;
    private final String TAG = "BDDInfo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);

        intent = getIntent();
        bundle = intent.getExtras();

        user = (User)bundle.getSerializable("USER");
        ((TextView)findViewById(R.id.usernameView)).setText(user.getFirstName()+ " " +user.getLastName());
        ((TextView)findViewById(R.id.scoreView)).setText(String.valueOf(user.getScore()));

        fetchUserScores();

    }

    public void startGame(View view){
        Intent intent = new Intent(GameMenu.this,Level1.class);
        Bundle bundle = new Bundle();
        switch(view.getId()){
            case R.id.normal_mode_button:
                bundle.putIntArray("Blinking_Times",new int[]{1,10});
                bundle.putInt("Lives",2);
                bundle.putFloat("Weight",1f);
                break;
            case R.id.hard_mode_button:
                bundle.putIntArray("Blinking_Times",new int[]{3,15});
                bundle.putInt("Lives",2);
                bundle.putFloat("Weight",1.5f);
                break;
            case R.id.expert_mode_button:
                bundle.putIntArray("Blinking_Times",new int[]{5,20});
                bundle.putInt("Lives",3);
                bundle.putFloat("Weight",3f);
                break;
            default:
                Toast.makeText(getApplicationContext(),"ERROR",Toast.LENGTH_LONG).show();
                throw new IllegalArgumentException();
        }
        bundle.putSerializable("User",user);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void fetchUserScores(){
        leaderboard = new ArrayList<User>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                leaderboard.add(new User(document.get("lastName").toString(),document.get("firstName").toString(),document.get("birthdate").toString(),document.get("genre").toString(),Float.parseFloat(document.get("score").toString())));
                            }
                            Collections.sort(leaderboard);
                            Collections.reverse(leaderboard);
                            ArrayList<Rank> ranked = ClassifyUsers();

                            recyclerView =findViewById(R.id.leaderboardEntries);
                            rankAdapter =new RankAdapter(ranked);

                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            recyclerView.setAdapter(rankAdapter);
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private ArrayList<Rank> ClassifyUsers(){
        ArrayList<Rank> ranked = new ArrayList<Rank>();
        ranked.add(new Rank(1,leaderboard.get(0)));
        for(int i=1;i<Math.min(leaderboard.size(),10);i++){
            User u = leaderboard.get(i);
            if(u.getScore()==ranked.get(i-1).getScore())
                ranked.add(new Rank(ranked.get(i-1).getRank(),u));
            else
                ranked.add(new Rank(i+1,u));
        }
        return ranked;
    }
}