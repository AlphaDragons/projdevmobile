package com.example.projdevmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.SigningInfo;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicSignUp(View view){
        startActivity(new Intent(MainActivity.this,SignUp.class));
    }

    public void clicSignIn(View view){
        startActivity(new Intent(MainActivity.this, ConnexionActivity.class));
    }
}
