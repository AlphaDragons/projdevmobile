package com.example.projdevmobile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.N)
public class Level3 extends GameMechanics {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);
        level=3;
        initGame();
    }

    @Override
    protected Intent NewLevel(String whatNext){
        try {
            Intent intent;
            switch (whatNext) {
                case "next":
                    intent = new Intent(Level3.this, Level4.class);
                    break;
                case "restart":
                    intent = new Intent(Level3.this, Level3.class);
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            Bundle bundle = new Bundle();
            bundle.putIntArray("Blinking_Times", new int[]{blinkMin, blinkMax});
            bundle.putInt("Lives", livesMax);
            bundle.putFloat("Weight", coeff);
            bundle.putSerializable("User",user);
            intent.putExtras(bundle);
            return intent;
        }
        catch(IllegalArgumentException exception){
            return null;
        }
    }
}