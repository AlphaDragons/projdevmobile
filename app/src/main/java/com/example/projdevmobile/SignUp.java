package com.example.projdevmobile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.datepicker.DateSelector;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {

    EditText lastname, firstname, email, password, birthdate;
    Button signup;
    RadioButton male, female;
    private FirebaseAuth mAuth;
    DatePickerDialog datePickerDialog;
    FirebaseFirestore db;
    String TAG = "BDDInfo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        lastname = findViewById(R.id.LastNameSignUp);
        firstname = findViewById(R.id.FirstNameSignUp);
        email = findViewById(R.id.EmailAdressSignUp);
        password = findViewById(R.id.PasswordSignUp);
        birthdate = findViewById(R.id.BirthDate);
        male = findViewById(R.id.MaleRadio);
        female = findViewById(R.id.FemaleRadio);
        signup = findViewById(R.id.SignUpButton);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        male.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    female.setChecked(false);
                }
        });

        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                male.setChecked(false);
            }
        });

        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();

                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(SignUp.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                birthdate.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!checkEntries()) return;
                    addUser(email.getText().toString(),password.getText().toString());
                }
            }
        );
    }

    boolean checkEntries()
    {
        if(lastname.getText().toString() == "")
        {
            Toast.makeText(SignUp.this, "Champ manquant : Nom de famille",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(firstname.getText().toString() == "")
        {
            Toast.makeText(SignUp.this, "Champ manquant : Prénom",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(email.getText().toString() == "")
        {
            Toast.makeText(SignUp.this, "Champ manquant : Mail",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString() == "")
        {
            Toast.makeText(SignUp.this, "Champ manquant : Mot de passe",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(birthdate.getText().toString() == "")
        {
            Toast.makeText(SignUp.this, "Champ manquant : Date de naissance",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!male.isChecked() && !female.isChecked())
        {
            Toast.makeText(SignUp.this, "Champ manquant : Sexe",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!isValidEmail(email.getText().toString()))
        {
            Toast.makeText(SignUp.this, "Adresse mail invalide",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().length()<6)
        {
            Toast.makeText(SignUp.this, "Mot de passe trop court. Il doit faire au moins 6 caractères",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    void addUser(String email, String password)
    {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information


                            addUserInfo(mAuth.getUid());
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUp.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });

    }

    void addUserInfo(String id)
    {
        Map<String, Object> user = new HashMap<>();
        user.put("lastName", lastname.getText().toString());
        user.put("firstName", firstname.getText().toString());
        user.put("birthdate",birthdate.getText().toString());
        user.put("genre",(male.isChecked() ? "Male" : "Female"));
        user.put("score", 0f);

        db.collection("users").document(id)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        Intent intent = new Intent(SignUp.this,ConnexionActivity.class);
                        startActivity(intent);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }
}