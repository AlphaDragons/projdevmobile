package com.example.projdevmobile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.N)
public class Level7 extends GameMechanics {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level7);
        level=7;
        initGame();
    }

    @Override
    protected Intent NewLevel(String whatNext){
            Intent intent=new Intent(Level7.this, Level7.class);
            Bundle bundle = new Bundle();
            bundle.putIntArray("Blinking_Times", new int[]{blinkMin, blinkMax});
            bundle.putInt("Lives", livesMax);
            bundle.putFloat("Weight", coeff);
            bundle.putSerializable("User",user);
            intent.putExtras(bundle);
            return intent;
    }
}