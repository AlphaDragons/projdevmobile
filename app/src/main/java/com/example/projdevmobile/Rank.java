package com.example.projdevmobile;

public class Rank{
    private int rank;
    private String firstname;
    private String lastname;
    private float score;

    public Rank(int rank,User user){
        this.rank = rank;
        firstname=user.getFirstName();
        lastname=user.getLastName();
        score=user.getScore();
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}
