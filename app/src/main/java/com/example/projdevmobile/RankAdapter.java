package com.example.projdevmobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RankAdapter extends RecyclerView.Adapter<RankHolder> {

    private ArrayList<Rank> userList;

    public RankAdapter(ArrayList<Rank> userList) {
        this.userList = userList;
    }

    @NonNull
    @Override
    public RankHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater monLayoutInflater = LayoutInflater.from(parent.getContext());
        View maView = monLayoutInflater.inflate(R.layout.leaderboard_entry,parent,false);
        return new RankHolder(maView);
    }

    @Override
    public void onBindViewHolder(@NonNull RankHolder holder, int position) {
        holder.ViewUser(userList.get(position));
    }

    @Override
    public int getItemCount() { //Basically useless
        return userList.size();
    }
}
