package com.example.projdevmobile;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RankHolder extends RecyclerView.ViewHolder {

    private TextView firstNameTV;
    private TextView lastNameTV;
    private TextView scoreTV;
    private TextView rankTV;

    public RankHolder(@NonNull View itemView) {
        super(itemView);

        firstNameTV = itemView.findViewById(R.id.id_prenom_view);
        lastNameTV = itemView.findViewById(R.id.id_nom_view);
        scoreTV = itemView.findViewById(R.id.id_score_view);
        rankTV = itemView.findViewById(R.id.id_rank_view);
    }

    public void ViewUser(Rank utilisateur){
        if(utilisateur!=null){
            firstNameTV.setText(utilisateur.getFirstname());
            lastNameTV.setText(utilisateur.getLastname());
            scoreTV.setText(String.valueOf(utilisateur.getScore()));
            rankTV.setText(String.valueOf(utilisateur.getRank())+".");
        }
    }
}
